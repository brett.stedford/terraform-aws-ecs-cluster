
# ECS-CLUSTER

## Description

This module will provision an AWS [ECS](https://docs.aws.amazon.com/AmazonECS/latest/developerguide/Welcome.html) Cluster and provision an auto scaling group with EC2 instances that will register with the cluster. The module will also create a security group and IAM role for the cluster. If desired the cluster can also be joined to an Active Directory domain.

The module assumes you are passing an AMI ID of either the [AWS ECS Optimised image](https://docs.aws.amazon.com/AmazonECS/latest/developerguide/ecs-optimized_AMI.html) or a customised image with the ECS agent/tools installed.

you will also need to provsion an [ALB](https://docs.aws.amazon.com/elasticloadbalancing/latest/application/introduction.html) or [NLB](https://docs.aws.amazon.com/elasticloadbalancing/latest/network/introduction.html) + secuirty group for load balancing purposes, this is not done by the module.

### Variables

- _envtype_ - The envtype is the type of environment the cluster will be deployed in.
- _team_ - Team name, primariy used for tagging.
- _project_ - sets the project name, used for tagging.
- _ecs\_cluster\_name_ - The name of the ECS Cluster.
- _desired\_capacity_ - The desired capacity of the ASG.
- _mini\_size_ - The minimum size of the ASG.
- _max\_size_ - the maximum size of the ASG.
- _health\_check\_type_ - The type of healthcheck for the ASG.
- _vpc\_zone\_identifier_ - The subnet ID's where the EC2 cluster nodes will be placed.
- _instance\_type_ - the EC2 Instance type that will be used for the cluster.
- _key\_name_ - the name of the EC2 Key Pair used to retrieve the administrator password through the AWS API.
- _spot\_price_ - If set the ASG will use spot instances at the set bid price, if not on demand instances are used.
- _domain\_name\_servers\CIDR_ - DNS servers for the AD domain.
- _root\_volume\_size_ - Sets the root volume size for the containers instances.
- _volume\_type_ - Set the volume type for the container instances.
- _aws\_alb\_sg\_id_ - The Security Group ID of the ALB that will front traffic for the ECS Instances.
- _vpc\_id_ - The ID of the VPC the cluster will be placed in.
- _ecs\_agent\_version_ - The version of the ECS agent to install.
- _associate\_public\_ip\_address_ - If enabled the EC2 instances will recieve public IP addresses.
- _ami\_name_ - The AMI to use for the EC2 Cluster Nodes.
- _ssm\_managed_ - If enabled the SSM managed policy will be applied to the cluster nodes.
- _cpu\_unbounded_ - Allows tasks set to 0 cpu units to be placed on a container instance.
- _region_ - The region that will be set as the shell default on the instance.
- _ssm\_param\_domain\_join\_password_ - The name of the SSM parameter that contains the password that wil be used to authenticate the domain join.
- _domain\_name_ - The name of the AD Domain to join.
- _join\_domain_ - Set if the the cluster is part of the Active Directory Domain.
- _account\_ou_ - The name of the OU that the computer account will be created in. Defined in RFC1779 format.
- _isLinux_ - If enabled then depicts that the ECS cluster is using Linux.
- _owners_ - Owners is a list of AMI owners to limit the search to. Defaults to searching Amazon AMIs.
- _ecs\_log\_level_ - The logging level specified for ecs logging (currently only affects linux). Defaults to info.
- _tags_ - A map of tags to add to all resources.

### Examples

#### Creating an ECS Cluster

```Ruby
module "ecs_cluster" {
  source                         = "../modules/ecs-cluster"
  domain_name_servers_CIDR       = ["10.10.10.1/32", "10.10.11.1/32", "10.10.12.1/32", "10.10.13.1/32"]
  domain_name                    = "contso.com"
  volume_type                    = "gp2"
  team                           = "razor"
  ecs_cluster_name               = "razor-qa-cluster"
  vpc_zone_identifier            = ["subnet-000a00000000000", "subnet-111b11111111111"]
  instance_type                  = "t3.small"
  key_name                       = "acccess-keypair"
  ecs_agent_version              = "v1.29.0"
  aws_alb_sg_id                  = "sg-00a00000aa000a0a0a"
  vpc_id                         = "vpc-00aaa0a00000000a"
  desired_capacity               = 2
  min_size                       = 0
  max_size                       = 2
  associate_public_ip_address    = 0
  ami_name                       = "ami-05da69b2d804943e6"
  ssm_managed                    = 1

    tags = {
    Terraform = "true"
    Environment = "dev"
  }
}
```

#### Creating an AD ECS Cluster

```Ruby
module "ad_ecs_cluster" {
  source                         = "../modules/ecs-cluster"
  domain_name_servers_CIDR       = ["10.10.10.1/32", "10.10.11.1/32", "10.10.12.1/32", "10.10.13.1/32"]
  domain_name                    = "contso.com"
  volume_type                    = "gp2"
  team                           = "razor"
  ecs_cluster_name               = "razor-qa-cluster"
  vpc_zone_identifier            = ["subnet-000a00000000000", "subnet-111b11111111111"]
  instance_type                  = "t3.small"
  key_name                       = "acccess-keypair"
  ecs_agent_version              = "v1.29.0"
  aws_alb_sg_id                  = "sg-00a00000aa000a0a0a"
  vpc_id                         = "vpc-00aaa0a00000000a"
  desired_capacity               = 2
  min_size                       = 0
  max_size                       = 2
  associate_public_ip_address    = 0
  ami_name                       = "ami-05da69b2d804943e6"
  ssm_managed                    = 1
  join_domain                    = "1"
  ssm_param_domain_join_user     = "domain-join-user"
  ssm_param_domain_join_password = "domain-join-password"
  account_ou                     = "DC=contso,DC=com"

    tags = {
    Terraform = "true"
    Environment = "dev"
  }
}
```

#### Creating a Linux ECS Cluster

```Ruby
module "ad_ecs_cluster" {
  source                         = "../modules/ecs-cluster"
  domain_name_servers_CIDR       = ["10.10.10.1/32", "10.10.11.1/32", "10.10.12.1/32", "10.10.13.1/32"]
  domain_name                    = "contso.com"
  volume_type                    = "gp2"
  team                           = "razor"
  ecs_cluster_name               = "razor-qa-cluster"
  vpc_zone_identifier            = ["subnet-000a00000000000", "subnet-111b11111111111"]
  instance_type                  = "t3.small"
  key_name                       = "acccess-keypair"
  aws_alb_sg_id                  = "sg-00a00000aa000a0a0a"
  vpc_id                         = "vpc-00aaa0a00000000a"
  desired_capacity               = 2
  min_size                       = 0
  max_size                       = 2
  associate_public_ip_address    = 0
  ami_name                       = "ami-05da69b2d804943e6"
  ssm_managed                    = 1
  isLinux                        = 1

    tags = {
    Terraform = "true"
    Environment = "dev"
  }
}
```

### TODO

- Add Cred Spec file support for applications that require AD authentication
