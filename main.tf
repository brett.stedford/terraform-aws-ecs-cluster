data "aws_caller_identity" "current" {}

data "template_file" "ecs" {
  template = "${var.isLinux == 0 ? 
              file("${path.module}/ecs.tmpl") : 
              file("${path.module}/ecs-linux.tmpl")}"

  vars {
    ecs_cluster_name  = "${var.ecs_cluster_name}"
    ecs_agent_version = "${var.ecs_agent_version}"
    cpu_unbounded     = "${var.cpu_unbounded}"
  }
}

data "template_file" "domain_connect" {
  template = "${file("${path.module}/domain_connect.tmpl")}"

  vars {
    region                          = "${var.region}"
    ssm_param_domain_join_user      = "${var.ssm_param_domain_join_user}"
    ssm_param_domain_join_password  = "${var.ssm_param_domain_join_password}"
    domain_name                     = "${var.domain_name}"
    account_ou                      = "${var.account_ou}"
  }
}

data "aws_ami" "ecs_optimised_windows" {
  most_recent = true
  owners      = "${var.owners}"

  filter {
    name   = "name"
    values = ["${var.ami_name}"]
  }
}

resource "aws_ecs_cluster" "cluster" {
  name = "${var.ecs_cluster_name}"
}

module "ecs_cluster_asg" {
  source  = "terraform-aws-modules/autoscaling/aws"
  version = "2.8.0"

  # Required Attributes
  desired_capacity    = "${var.desired_capacity}"
  min_size            = "${var.min_size}"
  max_size            = "${var.max_size}"
  health_check_type   = "${var.health_check_type}"
  name                = "${var.ecs_cluster_name}-asg"
  vpc_zone_identifier = "${var.vpc_zone_identifier}"

  # Optional attributes
  image_id                  = "${data.aws_ami.ecs_optimised_windows.image_id}"
  instance_type             = "${var.instance_type}"
  key_name                  = "${var.key_name}"
  security_groups           = "${split(",", var.join_domain != "0" ? join(",", flatten(list(aws_security_group.ecsInstance-ecs.*.id, aws_security_group.ads_ec2_on_prem_sg.*.id))) : aws_security_group.ecsInstance-ecs.id)}"
  spot_price                = "${var.spot_price}"
  user_data                 = "${var.join_domain == "1" ? format("<powershell>%s%s</powershell>", data.template_file.domain_connect.rendered, data.template_file.ecs.rendered) : format(var.isLinux == 0 ? "<powershell>%s</powershell>" : "%s", data.template_file.ecs.rendered)}"
  iam_instance_profile      = "${aws_iam_instance_profile.ecsInstance.id}"
  health_check_grace_period = 1500
  termination_policies      = ["OldestInstance", "Default"]
  associate_public_ip_address = "${var.associate_public_ip_address}"

  root_block_device = [
    {
      volume_size = "${var.root_volume_size}"
      volume_type = "${var.volume_type}"
    },
  ]

tags_as_map = "${var.tags}"
}

resource "aws_security_group" "ecsInstance-ecs" {
  name   = "${var.ecs_cluster_name} instance security group"
  vpc_id = "${var.vpc_id}"

tags = "${merge(map("Name", format("%s", var.name)), var.tags)}"
}

resource "aws_security_group_rule" "ecsInstance-ecs-80-egress" {
  protocol          = "TCP"
  type              = "egress"
  from_port         = 80
  to_port           = 80
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = "${aws_security_group.ecsInstance-ecs.id}"
}

resource "aws_security_group_rule" "ecsInstance-ecs-443-egress" {
  protocol          = "TCP"
  type              = "egress"
  from_port         = 443
  to_port           = 443
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = "${aws_security_group.ecsInstance-ecs.id}"
}

resource "aws_security_group_rule" "ecs-all-ingress" {
  protocol                 = "TCP"
  type                     = "ingress"
  from_port                = 0
  to_port                  = 65535
  security_group_id        = "${aws_security_group.ecsInstance-ecs.id}"
  source_security_group_id = "${var.aws_alb_sg_id}"
}

resource "aws_security_group_rule" "devopscluster_instances_out" {
  type                     = "egress"
  protocol                 = "tcp"
  from_port                = 0
  to_port                  = 65535
  security_group_id        = "${var.aws_alb_sg_id}"
  source_security_group_id = "${aws_security_group.ecsInstance-ecs.id}"
}

resource "aws_security_group_rule" "ecsInstance-rdp-ingress" {
  protocol          = "TCP"
  type              = "ingress"
  from_port         = 3389
  to_port           = 3389
  security_group_id = "${aws_security_group.ecsInstance-ecs.id}"
  cidr_blocks       = ["10.10.60.0/23", "10.10.70.0/23", "10.250.250.0/24", "109.111.215.168/32"]
}

resource "aws_security_group_rule" "ecsInstance_dns_tcp" {
  type              = "egress"
  from_port         = 53
  to_port           = 53
  protocol          = "tcp"
  cidr_blocks       = ["${var.domain_name_servers_CIDR}"]
  security_group_id = "${aws_security_group.ecsInstance-ecs.id}"
  description       = "allow tcp 53 outbound to DNS servers"
}

resource "aws_security_group_rule" "ecsInstance_dns_udp" {
  type              = "egress"
  from_port         = 53
  to_port           = 53
  protocol          = "udp"
  cidr_blocks       = ["${var.domain_name_servers_CIDR}"]
  security_group_id = "${aws_security_group.ecsInstance-ecs.id}"
  description       = "Allow UDP 53 outbound to DNS servers"
}

resource "aws_security_group" "ads_ec2_on_prem_sg" {
  count = "${var.join_domain}"
  name        = "${var.ecs_cluster_name} ads-ec2-on-prem"
  vpc_id      = "${var.vpc_id}"
  description = "${var.ecs_cluster_name} AD security group"
}

resource "aws_security_group_rule" "egress-1024-65535-TCP" {
  count = "${var.join_domain != "0" ? 1 : 0}"
  type              = "egress"
  protocol          = "TCP"
  from_port         = "49152"
  to_port           = "65535"
  security_group_id = "${aws_security_group.ads_ec2_on_prem_sg.id}"
  cidr_blocks       = ["${var.domain_name_servers_CIDR}"]
}

resource "aws_security_group_rule" "egress-1024-65535-UDP" {
  count = "${var.join_domain != "0" ? 1 : 0}"
  type              = "egress"
  protocol          = "UDP"
  from_port         = "49152"
  to_port           = "65535"
  security_group_id = "${aws_security_group.ads_ec2_on_prem_sg.id}"
  cidr_blocks       = ["${var.domain_name_servers_CIDR}"]
}

resource "aws_security_group_rule" "egress-123-123-UDP" {
  count = "${var.join_domain != "0" ? 1 : 0}"
  type              = "egress"
  protocol          = "UDP"
  from_port         = "123"
  to_port           = "123"
  security_group_id = "${aws_security_group.ads_ec2_on_prem_sg.id}"
  cidr_blocks       = ["${var.domain_name_servers_CIDR}"]
}

resource "aws_security_group_rule" "egress-135-135-TCP" {
  count = "${var.join_domain != "0" ? 1 : 0}"
  type              = "egress"
  protocol          = "TCP"
  from_port         = "135"
  to_port           = "135"
  security_group_id = "${aws_security_group.ads_ec2_on_prem_sg.id}"
  cidr_blocks       = ["${var.domain_name_servers_CIDR}"]
}

resource "aws_security_group_rule" "egress-389-389-TCP" {
  count = "${var.join_domain != "0" ? 1 : 0}"
  type              = "egress"
  protocol          = "TCP"
  from_port         = "389"
  to_port           = "389"
  security_group_id = "${aws_security_group.ads_ec2_on_prem_sg.id}"
  cidr_blocks       = ["${var.domain_name_servers_CIDR}"]
}

resource "aws_security_group_rule" "egress-389-389-UDP" {
  count = "${var.join_domain != "0" ? 1 : 0}"
  type              = "egress"
  protocol          = "UDP"
  from_port         = "389"
  to_port           = "389"
  security_group_id = "${aws_security_group.ads_ec2_on_prem_sg.id}"
  cidr_blocks       = ["${var.domain_name_servers_CIDR}"]
}

resource "aws_security_group_rule" "egress-445-445-TCP" {
  count = "${var.join_domain != "0" ? 1 : 0}"
  type              = "egress"
  protocol          = "TCP"
  from_port         = "445"
  to_port           = "445"
  security_group_id = "${aws_security_group.ads_ec2_on_prem_sg.id}"
  cidr_blocks       = ["${var.domain_name_servers_CIDR}"]
}

resource "aws_security_group_rule" "egress-53-53-TCP" {
  count = "${var.join_domain != "0" ? 1 : 0}"
  type              = "egress"
  protocol          = "TCP"
  from_port         = "53"
  to_port           = "53"
  security_group_id = "${aws_security_group.ads_ec2_on_prem_sg.id}"
  cidr_blocks       = ["${var.domain_name_servers_CIDR}"]
}

resource "aws_security_group_rule" "egress-53-53-UDP" {
  count = "${var.join_domain != "0" ? 1 : 0}"
  type              = "egress"
  protocol          = "UDP"
  from_port         = "53"
  to_port           = "53"
  security_group_id = "${aws_security_group.ads_ec2_on_prem_sg.id}"
  cidr_blocks       = ["${var.domain_name_servers_CIDR}"]
}

resource "aws_security_group_rule" "egress-88-88-TCP" {
  count = "${var.join_domain != "0" ? 1 : 0}"
  type              = "egress"
  protocol          = "TCP"
  from_port         = "88"
  to_port           = "88"
  security_group_id = "${aws_security_group.ads_ec2_on_prem_sg.id}"
  cidr_blocks       = ["${var.domain_name_servers_CIDR}"]
}

resource "aws_security_group_rule" "egress-88-88-UDP" {
  count = "${var.join_domain != "0" ? 1 : 0}"
  type              = "egress"
  protocol          = "UDP"
  from_port         = "88"
  to_port           = "88"
  security_group_id = "${aws_security_group.ads_ec2_on_prem_sg.id}"
  cidr_blocks       = ["${var.domain_name_servers_CIDR}"]
}

resource "aws_security_group_rule" "ingress-1024-65535-TCP" {
  count = "${var.join_domain != "0" ? 1 : 0}"
  type              = "ingress"
  protocol          = "TCP"
  from_port         = "49152"
  to_port           = "65535"
  security_group_id = "${aws_security_group.ads_ec2_on_prem_sg.id}"
  cidr_blocks       = ["${var.domain_name_servers_CIDR}"]
}

resource "aws_security_group_rule" "ingress-123-123-UDP" {
  count = "${var.join_domain != "0" ? 1 : 0}"
  type              = "ingress"
  protocol          = "UDP"
  from_port         = "123"
  to_port           = "123"
  security_group_id = "${aws_security_group.ads_ec2_on_prem_sg.id}"
  cidr_blocks       = ["${var.domain_name_servers_CIDR}"]
}

resource "aws_security_group_rule" "ingress-135-135-TCP" {
  count = "${var.join_domain != "0" ? 1 : 0}"
  type              = "ingress"
  protocol          = "TCP"
  from_port         = "135"
  to_port           = "135"
  security_group_id = "${aws_security_group.ads_ec2_on_prem_sg.id}"
  cidr_blocks       = ["${var.domain_name_servers_CIDR}"]
}

resource "aws_security_group_rule" "ingress-139-139-TCP" {
  count = "${var.join_domain != "0" ? 1 : 0}"
  type              = "ingress"
  protocol          = "TCP"
  from_port         = "139"
  to_port           = "139"
  security_group_id = "${aws_security_group.ads_ec2_on_prem_sg.id}"
  cidr_blocks       = ["${var.domain_name_servers_CIDR}"]
}

resource "aws_security_group_rule" "ingress-445-445-TCP" {
  count = "${var.join_domain != "0" ? 1 : 0}"
  type              = "ingress"
  protocol          = "TCP"
  from_port         = "445"
  to_port           = "445"
  security_group_id = "${aws_security_group.ads_ec2_on_prem_sg.id}"
  cidr_blocks       = ["${var.domain_name_servers_CIDR}"]
}

resource "aws_security_group_rule" "ingress-88-88-TCP" {
  count = "${var.join_domain != "0" ? 1 : 0}"
  type              = "ingress"
  protocol          = "TCP"
  from_port         = "88"
  to_port           = "88"
  security_group_id = "${aws_security_group.ads_ec2_on_prem_sg.id}"
  cidr_blocks       = ["${var.domain_name_servers_CIDR}"]
}

resource "aws_security_group_rule" "ingress-88-88-UDP" {
  count = "${var.join_domain != "0" ? 1 : 0}"
  type              = "ingress"
  protocol          = "UDP"
  from_port         = "88"
  to_port           = "88"
  security_group_id = "${aws_security_group.ads_ec2_on_prem_sg.id}"
  cidr_blocks       = ["${var.domain_name_servers_CIDR}"]
}

resource "aws_iam_instance_profile" "ecsInstance" {
  name = "${aws_iam_role.ecsInstance.name}"
  role = "${aws_iam_role.ecsInstance.name}"
  path = "/"
}

resource "aws_iam_role" "ecsInstance" {
  name = "${var.envtype}-${var.ecs_cluster_name}-ecsInstanceRole"

  assume_role_policy = <<POLICY
{
  "Version": "2008-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
POLICY
}

data "aws_iam_policy" "AmazonEC2ContainerServiceforEC2Role" {
  arn = "arn:aws:iam::aws:policy/service-role/AmazonEC2ContainerServiceforEC2Role"
}

resource "aws_iam_role_policy_attachment" "ecsInstance" {
  policy_arn = "${data.aws_iam_policy.AmazonEC2ContainerServiceforEC2Role.arn}"
  role       = "${aws_iam_role.ecsInstance.name}"
}

data "aws_iam_policy" "ssm_interaction_policy" {
  arn = "arn:aws:iam::aws:policy/service-role/AmazonEC2RoleforSSM"
}

resource "aws_iam_role_policy_attachment" "ssm_policy" {
  policy_arn  = "${data.aws_iam_policy.ssm_interaction_policy.arn}"
  role        = "${aws_iam_role.ecsInstance.name}"
  count       = "${var.ssm_managed}"
}

resource "aws_iam_role_policy" "join_domain" {
  role    = "${aws_iam_role.ecsInstance.name}"
  name    = "join_domain"
  count   = "${var.join_domain == 0 ? 0 : 1}"
  policy  = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "VisualEditor0",
            "Effect": "Allow",
            "Action": "ssm:GetParameter",
            "Resource": [
                "arn:aws:ssm:${var.region}:${data.aws_caller_identity.current.account_id}:parameter/${var.ssm_param_domain_join_password}",
                "arn:aws:ssm:${var.region}:${data.aws_caller_identity.current.account_id}:parameter/${var.ssm_param_domain_join_user}"
            ]
        }
    ]
}
EOF
}
