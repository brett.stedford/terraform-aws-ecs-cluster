output "cluster_security_group_id" {
  value = "${aws_security_group.ecsInstance-ecs.id}"
}

output "ecs_cluster_asg_name" {
  value = "${module.ecs_cluster_asg.this_autoscaling_group_name}"
}