# Change Log

## v 0.3.0 (06/09/2019)

### Features

*N/A*

### Fixes

* Fixes issue with ASG tags causing an error on apply.

## v 0.2.0

### Features

*N/A*

### Fixes

* Fixes issue with invalid variable.

## v 0.1.0

### Features

* Initial Commit

### Fixes

*N/A*